class @1GaussianBlur : public Kernel<float>@ {
  Mask<float> mask;
  Accessor<uchar> input;
  size_t range;

public:
  GaussianBlur(IterationSpace<float> iter, Accessor<uchar> acc,
               Mask<float> mask, size_t range)
      : Kernel(iter), input(acc), mask(mask), range(range) {
    addAccessor(acc);
  }

  @2void kernel()@ {
    output() = @3convolve@(mask, HipaccSUM, [&]() {
                 return input(mask) * mask();
               });
  }
};
